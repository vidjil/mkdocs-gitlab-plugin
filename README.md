
# mkdocs-gitlab-plugin

Plugin for [MkDocs](https://www.mkdocs.org/).
Transform handles such as `#1234`, `%56`, `!789`, `&12` or `$34` into links to a gitlab repository,
given by the `gitlab_url` configuration option.
Before the `#`/`%`/`!`/`&`/`$` is needed either a space, a '(', or a '['.

